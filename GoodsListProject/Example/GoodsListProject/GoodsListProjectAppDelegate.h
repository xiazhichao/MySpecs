//
//  GoodsListProjectAppDelegate.h
//  GoodsListProject
//
//  Created by GoodsListProject on 08/31/2018.
//  Copyright (c) 2018 GoodsListProject. All rights reserved.
//

@import UIKit;

@interface GoodsListProjectAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
