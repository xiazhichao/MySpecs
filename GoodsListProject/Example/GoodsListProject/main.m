//
//  main.m
//  GoodsListProject
//
//  Created by GoodsListProject on 08/31/2018.
//  Copyright (c) 2018 GoodsListProject. All rights reserved.
//

@import UIKit;
#import "GoodsListProjectAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GoodsListProjectAppDelegate class]));
    }
}
