# GoodsListProject

[![CI Status](https://img.shields.io/travis/GoodsListProject/GoodsListProject.svg?style=flat)](https://travis-ci.org/GoodsListProject/GoodsListProject)
[![Version](https://img.shields.io/cocoapods/v/GoodsListProject.svg?style=flat)](https://cocoapods.org/pods/GoodsListProject)
[![License](https://img.shields.io/cocoapods/l/GoodsListProject.svg?style=flat)](https://cocoapods.org/pods/GoodsListProject)
[![Platform](https://img.shields.io/cocoapods/p/GoodsListProject.svg?style=flat)](https://cocoapods.org/pods/GoodsListProject)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GoodsListProject is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'GoodsListProject'
```

## Author

GoodsListProject, 1256511693@qq.com

## License

GoodsListProject is available under the MIT license. See the LICENSE file for more info.
